Travel API Client 
=================

Clone this repo and start it (on windows systems use the gradlew.bat file):

`./gradlew bootRun`

to list all tasks:

`./gradlew tasks`

To view the assignment (after starting the application) go to:

[http://localhost:9000/travel/index.html](http://localhost:9000/travel/index.html)

Additional Comment Added by Naveen
=====================================

Front End URLs (Develped in AngularJs)

Flight Serach URL 
http://localhost:9000/travel/findflight.html

Airports Information 

http://localhost:9000/travel/airportserach.html

User Request Response Statistics

http://localhost:9000/travel/autoselection.html (Not Completed)


Spring Boot REST Services 
==========================

Find Flight 

Method : POST 

http://localhost:9000/travel/search?source=ZRH&destination=AMS
o/p
{
  "source": "Zurich - Zurich Airport (ZRH), Switzerland",
  "destination": "Amsterdam - Schiphol (AMS), Netherlands",
  "price": 2695.26,
  "currency": "EUR"
}

 Search All Airport

Method : GET 
http://localhost:9000/travel/searchallairports

Search Airport Based on User Inputs

Method : GET 

http://localhost:9000/travel/searchairports?term=india

Search by airport Code 

Method : GET  
http://localhost:9000/travel/searchbycity?code=ZRH

Flight Stats
Method : GET
http://localhost:9000/travel/flightstats

Need to add below properties for OAuth in application.yml 

service:
    tokenName: 8aeced52-01a2-4c03-a5b8-c36b445f9046 (need to replace by latest access Token)
    mockurl: http://localhost:8080/