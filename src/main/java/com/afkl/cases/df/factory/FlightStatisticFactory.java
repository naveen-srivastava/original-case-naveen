package com.afkl.cases.df.factory;

import com.afkl.cases.df.model.FlightStatisticResponse;

public class FlightStatisticFactory {

    public static final FlightStatisticResponse stats = new FlightStatisticResponse();

    public static FlightStatisticResponse getStatistic(){
        return stats;
    }
}
