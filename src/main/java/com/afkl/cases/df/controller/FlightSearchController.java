package com.afkl.cases.df.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.afkl.cases.df.model.Location;
import com.afkl.cases.df.service.FlightSearchService;

@Controller
public class FlightSearchController {

	@Autowired
	private FlightSearchService flightSearchService;

	@Value("${service.mockurl}")
	String serviceUrl;

	@Value("${service.tokenName}")
	String accessToken;

    @PostMapping("/search")
	public ResponseEntity<?> search(@RequestParam("source") String origin, @RequestParam("destination") String destination) {
		try {
			return flightSearchService.findFare(origin, destination);
		} catch (Exception e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping(value = "/searchbycity")
	public ResponseEntity<?> searchByCity(@RequestParam String code) {
		try {
			Location loc = flightSearchService.findAirport(code);
			return new ResponseEntity<Location>(loc, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Exception>(e, HttpStatus.BAD_REQUEST);
		}
	}

	
	@GetMapping(value = "/searchairports")
	public  ResponseEntity<?>searchAirportsByReqParam(@RequestParam String term) {
		
		Collection<Resource<Location>> list = flightSearchService.findAllAirports(term);
		return new ResponseEntity<Collection<Resource<Location>>>(list , HttpStatus.OK); 
		}
	
	@GetMapping(value = "/searchallairports" )
	public  ResponseEntity<?>searchAllAirports() {
		
		Collection<Resource<Location>> list = flightSearchService.findAllAirports();
	 		return new ResponseEntity<Collection<Resource<Location>>>(list , HttpStatus.OK); 
		}

}
