package com.afkl.cases.df.controller;

import com.afkl.cases.df.model.Coordinates;
import com.afkl.cases.df.model.CoordinatesMixin;
import com.afkl.cases.df.model.Location;
import com.afkl.cases.df.model.LocationMixin;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class JacksonLocationsModule extends SimpleModule {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
    public void setupModule(SetupContext context) {
        context.setMixInAnnotations(Location.class, LocationMixin.class);
        context.setMixInAnnotations(Coordinates.class, CoordinatesMixin.class);
    }

}
