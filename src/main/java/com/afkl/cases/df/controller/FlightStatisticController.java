package com.afkl.cases.df.controller;

import com.afkl.cases.df.factory.FlightStatisticFactory;
import com.afkl.cases.df.model.FlightStatisticResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FlightStatisticController {

    @RequestMapping("/flightstats")
    public FlightStatisticResponse getStatistic(){
        return FlightStatisticFactory.getStatistic();
    }
}
