package com.afkl.cases.df.model;

public class FlightStatisticResponse {

	private int numberOfRequests;
	private int failedRequests;
	private int successRequests;
	private int failedRequests_4xx;
	private long avResponseTime;
	private long maxResponseTime;
	private long minResponseTime;

	public int getNumberOfRequests() {
		return numberOfRequests;
	}

	public int getFailedRequests() {
		return failedRequests;
	}

	public int getSuccessRequests() {
		return successRequests;
	}

	public long getAvResponseTime() {
		return avResponseTime;
	}

	public long getMaxResTime() {
		return maxResponseTime;
	}

	public long getMinResponseTime() {
		return minResponseTime;
	}

	public void setStats(long responseTime, int statusCode) {
		numberOfRequests = numberOfRequests + 1;
		
		if (statusCode == 200) {
			successRequests = successRequests + 1;
		}
		else if (statusCode == 404 || statusCode == 400) {
			failedRequests_4xx = failedRequests_4xx + 1;
		}
		else {
			failedRequests = failedRequests + 1;
		}

		avResponseTime = ((avResponseTime * numberOfRequests) + responseTime) / (numberOfRequests + 1);
		maxResponseTime = responseTime > maxResponseTime ? responseTime : maxResponseTime;
		minResponseTime = minResponseTime == 0 || responseTime < minResponseTime ? responseTime : minResponseTime;
	}

	public int getFailedRequests_4xx() {
		return failedRequests_4xx;
	}

	public void setFailedRequests_4xx(int failedRequests_4xx) {
		this.failedRequests_4xx = failedRequests_4xx;
	}

}
