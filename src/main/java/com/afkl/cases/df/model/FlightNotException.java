package com.afkl.cases.df.model;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.NOT_FOUND, reason="Flight Not Found") //404
public class FlightNotException extends Exception {

	private static final long serialVersionUID = -3332292346834265371L;

	public FlightNotException(int id){
		super("EmployeeNotFoundException with id="+id);
	}
}