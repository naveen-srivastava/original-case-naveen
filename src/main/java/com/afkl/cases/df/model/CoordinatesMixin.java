package com.afkl.cases.df.model;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class CoordinatesMixin {

    @JsonCreator
    CoordinatesMixin(@JsonProperty("latitude") double latitude,
                     @JsonProperty("longitude") double longitude){
    }

}
