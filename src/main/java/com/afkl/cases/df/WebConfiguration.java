package com.afkl.cases.df;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.afkl.cases.df.inceptors.StatisticsInceptor;

@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {
	@Autowired
	StatisticsInceptor statisticsInceptor;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/index.html").addResourceLocations("classpath:/static/index.html");
		registry.addResourceHandler("/airportsearch.html").addResourceLocations("classpath:/static/airportsearch.html");
		registry.addResourceHandler("/findflight.html").addResourceLocations("classpath:/static/findflight.html");
		registry.addResourceHandler("/userstats.html").addResourceLocations("classpath:/static/userstats.html");
		registry.addResourceHandler("/autoselection.html").addResourceLocations("classpath:/static/autoselection.html");
		registry.addResourceHandler("/style.css").addResourceLocations("classpath:/static/style.css");
		registry.addResourceHandler("/flightSearch.js").addResourceLocations("classpath:/static/flightSearch.js");
	}

	/*
	 * Capture User http request statistic
	 * */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(statisticsInceptor).addPathPatterns("/search");
		registry.addInterceptor(statisticsInceptor).addPathPatterns("/searchbycity");
		registry.addInterceptor(statisticsInceptor).addPathPatterns("/searchairports");	
	}
}
