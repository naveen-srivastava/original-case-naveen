package com.afkl.cases.df.service;

import java.util.concurrent.Callable;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

import com.afkl.cases.df.model.Location;


public class SearchTask implements Callable<Location> {

    private final UriComponents uriComponents;
    private final RestTemplate restTemplate;

    public SearchTask(UriComponents uriComponent, RestTemplate restTemplate) {
        this.uriComponents = uriComponent;
        this.restTemplate = restTemplate;
    }

    @Override
    public Location call() throws Exception {
        return  restTemplate.getForObject(uriComponents.toUriString(), Location.class);
    }
}
