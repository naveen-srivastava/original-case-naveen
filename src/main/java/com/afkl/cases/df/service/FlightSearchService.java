package com.afkl.cases.df.service;

import static org.springframework.hateoas.MediaTypes.HAL_JSON;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.hateoas.mvc.TypeConstrainedMappingJackson2HttpMessageConverter;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.afkl.cases.df.model.Fare;
import com.afkl.cases.df.model.FlightSearchResponse;
import com.afkl.cases.df.model.Location;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class FlightSearchService {

    @Autowired
    RestTemplate restTemplate;

    @Value("${service.mockurl}")
    String serviceUrl;

    @Value("${service.tokenName}")
    String accessToken;


    final ExecutorService executorService = Executors.newFixedThreadPool(10);

    public Collection<Resource<Location>> findAllAirports(String terminal){
    	
    	ResponseEntity<PagedResources<Resource<Location>>> list_of_airports = getRestTemplateWithHalMessageConverter().exchange(getAllAirportsBySearchUrilBuilder(terminal).toUriString(),HttpMethod.GET,  ResponseEntity.EMPTY, new ParameterizedTypeReference<PagedResources<Resource<Location>>>(){});
		
    	PagedResources<Resource<Location>> page_resource = (PagedResources<Resource<Location>>)list_of_airports.getBody();

    	Collection<Resource<Location>> content_list = page_resource.getContent();
    	   	
    	return  content_list;
    }
    
    
    public Collection<Resource<Location>> findAllAirports(){
    	
    	ResponseEntity<PagedResources<Resource<Location>>> list_of_airports = getRestTemplateWithHalMessageConverter().exchange(getAllAirportsUrilBuilder().toUriString(),HttpMethod.GET,  ResponseEntity.EMPTY, new ParameterizedTypeReference<PagedResources<Resource<Location>>>(){});
		
    	PagedResources<Resource<Location>> page_resource = (PagedResources<Resource<Location>>)list_of_airports.getBody();

    	Collection<Resource<Location>> content_list = page_resource.getContent();
    	   	
    	return  content_list;
    }
   
    public Location findAirport(String code){
        return restTemplate.getForObject(getAirportByCodeUrilBuilder(code).toUriString(), Location.class);
    }

    public ResponseEntity <?> findFare(String origin, String destination) throws ExecutionException,InterruptedException,Exception {

        UriComponentsBuilder fareBuilder = uriBuilder("/fares/{origin_code}/{destination_code}");
        Map<String, String> fareServiceParameter = new HashMap<>();
        fareServiceParameter.put("origin_code", origin);
        fareServiceParameter.put("destination_code", destination);
        final Future<Location> source = executorService.submit(new SearchTask(getAirportByCodeUrilBuilder(origin), restTemplate));
        final Future<Location> dest = executorService.submit(new SearchTask(getAirportByCodeUrilBuilder(destination), restTemplate));
        final Future<Fare> fare = executorService.submit(new FareTask(fareBuilder.buildAndExpand(fareServiceParameter), restTemplate));

        FlightSearchResponse flightSearchResponse = new FlightSearchResponse();
        flightSearchResponse.setSource(source.get().getDescription());
        flightSearchResponse.setDestination(dest.get().getDescription());
        flightSearchResponse.setPrice(fare.get().getAmount());
        flightSearchResponse.setCurrency(fare.get().getCurrency().name());
        return new ResponseEntity<FlightSearchResponse>(flightSearchResponse, HttpStatus.OK);
    }
    
   private UriComponents getAllAirportsBySearchUrilBuilder(String term){
        UriComponentsBuilder builder = uriBuilder("/airports");
        builder.queryParam("term", term);
        return builder.buildAndExpand();
    }
   
   private UriComponents getAllAirportsUrilBuilder(){
       UriComponentsBuilder builder = uriBuilder("/airports");
      // builder.queryParam("term", term);
       return builder.buildAndExpand();
   }

    private UriComponents getAirportByCodeUrilBuilder(String code){
        UriComponentsBuilder builder = uriBuilder("/airports/{code}");
        Map<String, String> uriParams = new HashMap<>();
        uriParams.put("code", code);
        return builder.buildAndExpand(uriParams);
    }
    
   private UriComponentsBuilder uriBuilder(String uri){
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(serviceUrl+uri);
         builder.queryParam("access_token", accessToken);
         return builder;
   }
   
  	private RestTemplate getRestTemplateWithHalMessageConverter() {

  		List<HttpMessageConverter<?>> existingConverters = restTemplate.getMessageConverters();
  		List<HttpMessageConverter<?>> newConverters = new ArrayList<>();
  		newConverters.add(getHalMessageConverter());
  		newConverters.addAll(existingConverters);
  		restTemplate.setMessageConverters(newConverters);
  		return restTemplate;
  	}

  	private HttpMessageConverter<?> getHalMessageConverter() {
  		ObjectMapper objectMapper = new ObjectMapper();
  		objectMapper.registerModule(new Jackson2HalModule());
  		MappingJackson2HttpMessageConverter halConverter = new TypeConstrainedMappingJackson2HttpMessageConverter(ResourceSupport.class);
  		halConverter.setSupportedMediaTypes(Arrays.asList(HAL_JSON));
  		halConverter.setObjectMapper(objectMapper);
  		return halConverter;
  	}

}
