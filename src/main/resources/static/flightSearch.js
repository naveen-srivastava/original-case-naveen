angular.module('flight-search-demo', ['ngAnimate', 'ngSanitize', 'ui.bootstrap']);
angular.module('flight-search-demo').controller('FlightSearchCtrl', function($scope, $http) {

  var inputMin = 3;
  
  $scope.disablePaymentButton = true;
  	  
  $scope.srcTextChanged = function() {
	  if ($scope.source.length >= inputMin)
		  callSrcRestService($scope.source)
  };
 
  function callSrcRestService(inputtext) {
	  $scope.srccode = {};
	  $http.get('/travel/searchbycity?code=' + inputtext)
	  .then(function(response) {
		  
		  if (response.status === 200){
			  $scope.disablePaymentButton = false;
			 $scope.source = response.data.name;
			 $scope.srccode = response.data.code; 
		      $scope.errorsrc = '';
		     }
		  else{
			  $scope.bkerror = response.data;
		  }
		            
	  }).catch(function(response) {
		  $scope.errorsrc = response.data;
		  $scope.disablePaymentButton = true;
      });
  };
  
  $scope.desttextChanged = function() {
	  $error = {};
	  console.log("inside Dest ")
      if ($scope.destination.length >= inputMin) callDestRestService($scope.destination)
  };
  
  function callDestRestService(inputtext) {
	  $http.get('/travel/searchbycity?code=' + inputtext)
	  .then(function(response) {
		  
		  if (response.status === 200){
			  $scope.destination = response.data.name;
			  $scope.descode = response.data.code;
			  $scope.disablePaymentButton = false;
			  $scope.errordest = '';
			 }
		  else{
			  $scope.bkerror = response.data;
		  }
	  }).catch(function(response) {
		  $scope.errordest = response.data;
		  $scope.disablePaymentButton = true;
      }); 
  };
  
  
  $scope.callFlighSearch = function() {
	  console.log('callflighsearch');
	
	  $http.post('/travel/search?'+'source='+$scope.srccode+'&destination='+$scope.descode)
	  .then(function(response) {
		  if (response.status === 200){
			  $scope.searchResult = response.data;
	      }
		  else{
			  $scope.bkerror = response.data;
		  }
	  }).catch(function(response) {
		  $scope.bkerror = response.data;
      });;
  };
  
  
  $scope.searchTextChanged = function() {
	  	if ($scope.searchString.length >= inputMin) 
    	  {
    	  	getAllFlightMatchWithQueryParam($scope.searchString);
    	  } else {
    		  $scope.searchAllResult ='';
    		  $scope.errorsearchResultflag = false;
    	  }
    	
  };
  
 $scope.errorsearchResultflag = false;
 
 function getAllFlightMatchWithQueryParam(searchString) {
	  console.log('getAllFlightMatchWithQueryParam');
		  $http.get('/travel/searchairports?'+'term='+searchString)
	  .then(function(response) {
		  if (response.status === 200){
			  $scope.searchAllResult = response.data;
			  $scope.errorsrcResult = '';
			  $scope.errorsearchResultflag = false;
	      }
		  else{
			  $scope.errorsearchResult = response.data;
			  $scope.errorsearchResultflag = true;
			 }
	  }).catch(function(response) {
		  $scope.errorsearchResult = response.data;
		  $scope.errorsearchResultflag = true;
		 
      });;
  };
  
  $scope.errorstatResultflag = false;
  
  $scope.getAllRequestStatistics =  function () {
	  console.log('getAllRequestStatistics');
		$http.get('/travel/flightstats')
	  .then(function(response) {
		  if (response.status === 200){
			  $scope.statResult = response.data;
		   }
		  else{
			  $scope.errorstatResult = response.data;
			  $scope.errorstatResultflag = true;
			 }
	  }).catch(function(response) {
		  $scope.errorstatResult = response.data;
		  $scope.errorstatResultflag = true;
		 
      });;
  }; 
  
  $scope.searchTextChangedAuto = function() {
	  console.log('searchTextChangedAuto');
	 
	  		getAllFlight();
  	  
  	
  };

  function getAllFlight() {
	  console.log('getAllFlight');
		 $http.get('/travel/searchairports?'+'term='+_selected)
		 // $http.get('/travel/searchallairports')  
	  .then(function(response) {
		  if (response.status === 200){
			  $scope.statesWithFlags = response.data;
			//  $scope.errorAllResult = '';
			 $scope.errorsearchAllResultflag = false;
	      }
		  else{
			//  $scope.errorAllResult = response.data;
			 $scope.errorsearchAllResultflag = true;
			 }
	  }).catch(function(response) {
		 // $scope.errorAllResult = response.data;
		  $scope.errorsearchAllResultflag = true;
		 
      });;
  };

 var _selected;

 $scope.selected = undefined;


$scope.ngModelOptionsSelected = function(value) {
  if (arguments.length) {
    _selected = value;
  } else {
    return _selected;
  }
};

$scope.modelOptions = {
  debounce: {
    default: 500,
    blur: 250
  },
  getterSetter: true
};










});